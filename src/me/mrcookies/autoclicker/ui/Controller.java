package me.mrcookies.autoclicker.ui;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.awt.*;
import java.util.Timer;

import static me.mrcookies.autoclicker.utility.Methods.*;

public class Controller {

    @FXML
    private TextField cps, keyword;
    @FXML
    private CheckBox check;

    private final Timer timer = new Timer();

    @FXML
    public void startAction() throws AWTException {

        if (isTextFieldNull(cps)) {
            showErrorMessage("Fields cannot be empty!");
            return;
        }

        int CPS = 0;

        try {
            CPS = Integer.parseInt(cps.getText());
        } catch (final NumberFormatException e) {
            showErrorMessage("Please enter a valid number in CPS field.");
        }

        showNotification("Auto Clicker", "Successfully started!", "me/mrcookies/autoclicker/resources/images/Notify.png");

        // timer.scheduleAtFixedRate(new TestTask(CPS), 0, 0);

    }


}
