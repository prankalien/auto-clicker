package me.mrcookies.autoclicker.tasks;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.TimerTask;

public class CPSTask extends TimerTask {

    private final int CPS;
    private static Robot robot;

    static {

        try {
            robot = new Robot();
        } catch (final AWTException e) {
            e.printStackTrace();
        }

    }

    public CPSTask(final int CPS) {
        this.CPS = CPS;
    }

    @Override
    public void run() {

        if (robot == null) return;

        for (int i = 0; i < CPS; i++) {
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mousePress(InputEvent.BUTTON1_MASK);
        }

    }

}
