package me.mrcookies.autoclicker.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage stage;

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final Parent root = FXMLLoader.load(getClass().getResource("Autoclicker.fxml"));
        primaryStage.setTitle("Auto Clicker");
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("me/mrcookies/autoclicker/resources/images/Icon.png"));
        primaryStage.setScene(new Scene(root, 300, 153));
        primaryStage.show();

        stage = primaryStage;
    }

    public static void main(final String[] args) {
        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }

}
