package me.mrcookies.autoclicker.utility;

import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.NotificationPane;
import org.controlsfx.control.Notifications;

public class Methods {

    public static boolean isTextFieldNull(final TextField textField) {
        return textField.getText().equals("");
    }

    public static void showErrorMessage(final String errorMessage) {
        final Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(errorMessage);
        alert.setResizable(false);

        final Stage a = (Stage) alert.getDialogPane().getScene().getWindow();

        a.getIcons().add(new Image("me/mrcookies/autoclicker/resources/images/Icon.png"));

        alert.showAndWait();
    }

    public static void showNotification(final String title, final String text) {
        final Notifications notificationsBuilder = Notifications.create()
                .title(title)
                .text(text)
                .graphic(null)
                .hideAfter(Duration.seconds(2))
                .position(Pos.TOP_RIGHT);

        notificationsBuilder.showInformation();
    }

    public static void showNotification(final String title, final String text, final String path) {

        final Image img = new Image(path);
        final NotificationPane notify = new NotificationPane();
        final String css = Methods.class.getResource("me/mrcookies/autoclicker/resources/css/Notification.css").toExternalForm();

        notify.getStylesheets().add(css);

        final Notifications notificationsBuilder = Notifications.create()
                .owner(notify)
                .title(title)
                .text(text)
                .graphic(new ImageView(img))
                .hideAfter(Duration.seconds(2))
                .hideCloseButton()
                .position(Pos.TOP_RIGHT);

        notificationsBuilder.show();
    }

}
